# !/usr/bin/python3
# coding: utf-8


"""
Take a look at this string of numbers:

   3 3 3 2 3 3 3 2 3 3 3 2 3 3 2 3 3 3 2 3 3 3 2 3 3 3 2 3 3 2 3 3 3 2 3 3 3 2

At first it looks like someone fell asleep on a keyboard. But there’s an
inner logic to the sequence. This sequence creates itself, number by number,
as explained in the image above. Each digit refers to the number of
consecutive 3s before a certain 2 appears. Specifically, the first digit
refers to the number of consecutive 3s that appear before the first 2,
the second digit refers to the number of 3s that appear consecutively before
the second 2, and so on toward infinity.

The sequence never ends, but that won’t stop us from asking us questions
about it. What is the ratio of 3s to 2s in the entire sequence?
"""

import matplotlib.pyplot as plt


def catch_divide(a, b):
    """
    :param a: float
        Dividend
    :param b: float
        Divisor
    :return: float
        Perform a / b and returns a iff anything goes wrong
    """

    try:
        return float(a / b)
    except:
        return a


def ramirez_till(n):
    """
    :param n: int
        Number to get values of sequence up to
    :return: [] of int
        List of a(x) for 1 <= x <= n
    """

    sequence = [3]  # a(1) = 3
    read_index = 0  # index where we read instructions on how continue seq

    while len(sequence) < n:
        threes_count = sequence[read_index]
        sequence += (threes_count - 1) * [3]  # already there is one 3s
        sequence += [2, 3]  # the 2 and the following 3
        read_index += 1  # read next instruction

    return sequence[: n]  # in case I added an extra item


def ramirez_threes_to_twos_ratio_till(n):
    """
    :param n: int
        Number to get values of sequence up to
    :return: [] of int, [] of float
        List of a(x) for 1 <= x <= n and list of twos(x) / ones(x) for 1 <=
        x <= n
    """

    sequence = [3]  # a(1) = 3
    read_index = 0  # index where we read instructions on how continue seq
    total_threes = 1  # 3s and 2s counters
    total_twos = 0
    threes_two_ratios = [
        catch_divide(total_threes, total_twos)
    ]  # first ratio is 0 / 1

    while len(sequence) < n:
        threes_count = sequence[read_index]
        for _ in range(threes_count - 1):  # already there is one 3s
            sequence.append(3)  # add new value
            total_threes += 1  # update counters and ratio
            threes_two_ratios.append(
                catch_divide(total_threes, total_twos)
            )

        sequence.append(2)  # add following 2
        total_twos += 1  # update counters and ratio
        threes_two_ratios.append(
            catch_divide(total_threes, total_twos)
        )

        sequence.append(3)  # add following 2
        total_threes += 1  # update counters and ratio
        threes_two_ratios.append(
            catch_divide(total_threes, total_twos)
        )

        read_index += 1  # read next instruction

    return sequence[: n], threes_two_ratios[: n]


def main():
    n = 10000000  # generate sequence until this number
    ramirez_sequence, three_twos_ratio = ramirez_threes_to_twos_ratio_till(n)
    plt.plot(range(1, n + 1), three_twos_ratio)

    plt.xlabel("n")
    plt.ylabel("Ratio between (number of 3s) and (number of 2s)")
    plt.title("ramirez sequence 3s to 2s ratio")
    plt.grid(True)
    plt.show()  # plot


if __name__ == "__main__":
    main()
