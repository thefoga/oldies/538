# Can You Unravel These Number Strings?

> From Brad Ramirez, a second shrewd sequence:
Take a look at this string of numbers:

```
3 3 3 2 3 3 3 2 3 3 3 2 3 3 2 3 3 3 2 3 3 3 2 3 3 3 2 3 3 2 3 3 3 2 3 3 3 2
```

> At first it looks like someone fell asleep on a keyboard. But there’s an inner logic to the sequence. This sequence creates itself, number by number, as explained in the image above. Each digit refers to the number of consecutive 3s before a certain 2 appears. Specifically, the first digit refers to the number of consecutive 3s that appear before the first 2, the second digit refers to the number of 3s that appear consecutively before the second 2, and so on toward infinity.

> 1. The sequence never ends, but that won’t stop us from asking us questions about it. What is the ratio of 3s to 2s in the entire sequence?

See charts below: the `x-axis` is the size of the sequence, and the `y-axis`
 is the ratio between the number of `3` and `2`.

![all](img/all.png)
![end](img/end.png)
![1e7](img/1e7.png)

which yield as `~2.7320525 = 1 + sqrt(3)` the limit of such ratio.
