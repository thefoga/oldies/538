# Who Steals The Most In A Town Full Of Thieves?

> A town of 1,000 households has a strange law intended to prevent
wealth-hoarding. On January 1 of every year, each household robs one other
household, selected at random, moving all of that house’s money into their
own house. The order in which the robberies take place is also random and is
determined by a lottery. (Note that if House A robs House B first, and then
C robs A, the houses of A and B would each be empty and C would have
acquired the resources of both A and B.). Two questions about this fateful day:

> 1. What is the probability that a house is not robbed over the course of the
day?

See charts below: the `x-axis` is the number 
of households in the simulated neighbourhood, and the `y-axis` is the 
probability of a house not being robbed

![Simulations until 5k households](img/prob-not-being-robbed/5k.png)

which yield as `average` probability of a house not being robbed about `0.367879441... = 1 / e`.

> 2. Suppose that every house has the same amount of cash to begin with -
say $100. Which position in the lottery has the most expected cash at the
end of the day, and what is that amount?

See charts below: the `x-axis` is the number 
of households in the simulated neighbourhood, and the `y-axis` is the 
average position of the richest people at the end of the day of robberies

![Simulations until 5k households](img/richest-index/5k.png)

which shows that the richest people are always in the middle of the households.
