# !/usr/bin/python3
# coding: utf-8


"""
A town of 1,000 households has a strange law intended to prevent
wealth-hoarding. On January 1 of every year, each household robs one other
household, selected at random, moving all of that house’s money into their
own house. The order in which the robberies take place is also random and is
determined by a lottery. (Note that if House A robs House B first, and then
C robs A, the houses of A and B would each be empty and C would have
acquired the resources of both A and B.)

Two questions about this fateful day:

1 - What is the probability that a house is not robbed over the course of the
day?
2 - Suppose that every house has the same amount of cash to begin with -
say $100. Which position in the lottery has the most expected cash at the
end of the day, and what is that amount?
"""

import random

import numpy as np
from hal.charts.plotter import Plot2d


def are_valid_robberies(robberies_list):
    """
    :param robberies_list: list of int
        List of house to rob for each house
    :return: bool
        True iff there is nobody robbing oneself
    """

    for house, robber in enumerate(robberies_list):
        if house == robber:
            return False
    return True


def get_lottery_robberies(n_houses):
    """
    :param n_houses: int
        Number of houses in neighbourhood
    :return: dict <int, int>
        Keys are the houses, values are the house to rob
    """

    robberies = [random.randint(0, n_houses - 1) for _ in range(n_houses)]
    while not are_valid_robberies(robberies):
        robberies = [random.randint(0, n_houses - 1) for _ in range(n_houses)]

    return {
        house: to_rob for house, to_rob in enumerate(robberies)
        }


def simulate(n_houses, cash, n_simulation_rounds=1000):
    """
    :param n_houses: int
        Number of houses in neighbourhood
    :param cash: list of float
        List of cash of each house
    :param n_simulation_rounds: int
        Number of simulation rounds to do
    :return: tuple (float, int)
        Probability being robbed and wealthiest lottery position
    """

    # probability of house not robbed in each simulation
    not_robbed_count = [0.0] * n_houses
    wealthiest = [-1] * n_simulation_rounds

    for sim_round in range(n_simulation_rounds):
        robberies = get_lottery_robberies(n_houses)
        has_been_robbed = [False] * n_houses

        for house in robberies:  # np.random.permutation(list(robberies.keys(
            # ))):
            house_to_rob = robberies[house]  # target house
            has_been_robbed[house_to_rob] = True

            cash[house] += cash[house_to_rob]  # take the money ...
            cash[house_to_rob] = 0  # ... and run

        for house in range(n_houses):
            if not has_been_robbed[house]:
                not_robbed_count[house] += 1.0  # increase not-robbed counter

        wealthiest[sim_round] = cash.index(max(cash))  # get index of richest

    not_robbed_count = [
        p / n_simulation_rounds for p in not_robbed_count
        ]
    return np.average(not_robbed_count), np.average(wealthiest)


def plot_results():
    """
    :return: void
        Plot results of different simulations
    """

    simulations = range(20, 5000, 20)
    not_being_robbed_prob = [0.0] * len(simulations)
    richest_people = [-1] * len(simulations)

    for i, houses in enumerate(simulations):
        prob, pos = simulate(
            houses,
            [100] * houses,
            n_simulation_rounds=100
        )

        not_being_robbed_prob[i] = prob
        richest_people[i] = pos

        percentage_done = 100.0 * i / len(simulations)
        print("Simulated", houses, "houses in neighbourhood [",
              percentage_done, "% ]")

    print("Done", len(simulations), "simulations.")
    print(
        "Average probability of not being robbed is",
        np.average(not_being_robbed_prob)
    )
    print(
        "Average of richest people / number of houses is",
        np.average(
            [
                richest_people[i] / simulations[i]
                for i in range(len(richest_people))
                ]
        )
    )

    # plot simulations
    Plot2d.scatter(simulations, not_being_robbed_prob)
    Plot2d.scatter(simulations, richest_people)


if __name__ == '__main__':
    plot_results()
