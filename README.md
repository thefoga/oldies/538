<div align="center">
<h1>538</h1>
<em>A collection of my solutions of the <a href="https://fivethirtyeight.com">FiveThirtyEight</a> <a href="https://fivethirtyeight.com/tag/the-riddler/">riddlers</a></em></br></br>
</div>

<div align="center">
<a href="https://opensource.org/licenses/Apache-2.0"><img alt="Open Source Love" src="https://badges.frapsoft.com/os/v1/open-source.svg?v=103"></a>
<a href="https://github.com/sirfoga/538/issues"><img alt="Contributions welcome" src="https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat"></a>
<a href="https://www.apache.org/licenses/LICENSE-2.0"><img alt="License" src="https://img.shields.io/badge/license-Apache%202.0-blue.svg"></a>
</div>


## Riddlers implemented

| Date | Title and link | Solution |
|:--------------|:----------------:|:----------------:|
| APR. 6, 2018 | [When Will The Arithmetic Anarchists Attack?](hhttps://fivethirtyeight.com/features/when-will-the-arithmetic-anarchists-attack/)| ... |
| MAR. 23, 2018 | [Can You Shuffle Numbers? Can You Find All The World Cup Results?](https://fivethirtyeight.com/features/can-you-shuffle-numbers-can-you-find-all-the-world-cup-results/)| ... |
| OCT. 20, 2017 | [Can You Please The Oracle? Can You Escape The Prison?](https://fivethirtyeight.com/features/can-you-please-the-oracle-can-you-escape-the-prison/)| ... |
| AGO. 19, 2017 | [Can You Unravel These Number Strings?](https://fivethirtyeight.com/features/can-you-unravel-these-number-strings/)| [Python](2017-08-18) |
| JUN. 30, 2017 | [Who Steals The Most In A Town Full Of Thieves?](https://fivethirtyeight.com/features/who-steals-the-most-in-a-town-full-of-thieves/)| [Python](2017-06-30) |


## Contributing
[Fork](https://github.com/sirfoga/538/fork) | Patch | Push | [Pull request](https://github.com/sirfoga/538/pulls)


## Feedback
Suggestions and improvements [welcome](https://github.com/sirfoga/538/issues)!


## Authors
| [![sirfoga](https://avatars0.githubusercontent.com/u/14162628?s=128&v=4)](https://github.com/sirfoga "Follow @sirfoga on Github") |
|---|
| [Stefano Fogarollo](https://sirfoga.github.io) |


## License
[Unlicense](https://unlicense.org/)
